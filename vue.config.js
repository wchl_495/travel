const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  lintOnSave: true,
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@$', resolve('src'))
      .set('styles', resolve('src/assets/styles'))
  },
  devServer: {
    // 设置主机地址
    // host: 'localhost',
    // 设置默认端口
    port: 8080,
    // 设置代理
    hotOnly: true, // 热更新
    proxy: {
      '/api/*': {
        // 目标 API 地址
        target: 'http://localhost:8080/',
        // 如果要代理 websockets
        ws: true,
        // 将主机标头的原点更改为目标URL
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/mock'
        },
        secure: false
      }
    }
  }
}
